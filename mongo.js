/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

*/
// Create - Users
db.users.insertMany(
	[
	{
		firstName : "Dean",
		lastName : "Winchester",
		email: "dean.winchester@supernatural.com",
		password: "qwerty",
		isAdmin: false
	},
	{
		firstName : "Sam",
		lastName : "Winchester",
		email: "sam.winchester@supernatural.com",
		password: "qwerty",
		isAdmin: false
	},
	{
		firstName : "Castiel",
		lastName : "",
		email: "castiel@supernatural.com",
		password: "qwerty",
		isAdmin: false
	},
	{
		firstName : "Fergus Roderick",
		lastName : "MacLeod",
		email: "crowley@supernatural.com",
		password: "qwerty",
		isAdmin: false
	},
	{
		firstName : "Chuck",
		lastName : "Shurley",
		email: "chuck.shurley@supernatural.com",
		password: "qwerty",
		isAdmin: false
	}
]
)


/*


	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

*/

// Create - Users
db.courses.insertMany(
	[
	{
		name : "Front-end Development",
		price : 18000,
		isActive : false
	},
	{
		name : "Back-end Development",
		price : 18000,
		isActive : false
	},
	{
		name : "Full-stack Development",
		price : 18000,
		isActive : false
	}
]
)



/*
//Read

	>> Find all regular/non-admin users.


*/
	db.users.find({isAdmin: false})

/*

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.


*/

	db.users.updateOne({}, 
			{
				$set: {
					isAdmin: true
				}
			}

		)

	db.courses.updateOne({}, 
			{
				$set: {
					isActive: true
				}
			}

		)

	


/*


//Delete

	>> Delete all inactive courses


*/
db.courses.deleteMany({isActive: false})

/*


//Add all of your query/commands here in activity.js

*/


